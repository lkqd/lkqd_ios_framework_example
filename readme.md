## Synopsis

This project is to show an example of how to use lkqd ad services inside your swift iOS application. 
The demo application is a simple app that will show an ad after a series of button presses.
It also demonstrates loading 2 ads back to back with different methods of starting them.
The demo app should have plenty of comments to follow along and understand what is being called and why. 
All end user functionality is demonstrated in ViewController.swift and any LKQD specific things should be marked as such.
Anything specifically for the demo that isn't needed should have this comment above it: //example app specific code

## Installation

LKQD Ads are built using swift version 2.2 and Xcode 7.3.1. 
Earlier versions might have compilation errors. 

An example of importing is shown here:
![alt tag](http://devflash.lkqd.com/img/lkqd_ios_install.gif)

The only files you should need is LKQDAD.framework and a bridging header file (example in LKQDBridgingHeader.h). 
Simply drag and drop LKQDAD.framework into your project and also check copy items if needed and create folder references. 
This should import the framework into the project but you still need to do a few things to get it to compile. 
The first is in the General tab under the project settings. Under "Embedded Binaries" select the + button to add the LKQDAD.framework file to the project. 
Once that is selected you should see LKQDAD.framework under "Linked Frameworks and Binaries". If it isn't you should add it there as well. 

To make sure the framework compiles you will need a bridging header file. 
The example app uses LKQDBridgingHeader.h and you can look at that for an example.

Learn more here:
https://developer.apple.com/library/ios/documentation/Swift/Conceptual/BuildingCocoaApps/MixandMatch.html
"In Build Settings, in Swift Compiler - Code Generation, make sure the Objective-C Bridging Header build setting under has a path to the bridging header file.
The path should be relative to your project, similar to the way your Info.plist path is specified in Build Settings."

So for the example app my "Objective-C Bridging Header" setting is set to: 
LKQDFrameworkTest/LKQDBridgingHeader.h

Once that is set the example app should be able to compile and import the framework tools. 
If you already have a bridging header file then you can skip adding the LKQDBridgingHeader.h file and just add the following line to yours once the framework is imported:

\#import "LKQDAD/LKQDAD.h" or wherever you end up putting the framework directory

When compiling for different devices you will need different versions of the framework. 
The iphoneOS version is used for when building to a device while iphonesimulator is used for building to a simulator.
The example app is currently using the universal version which can build to both. 
If you are trying to publish to the app store then it will not accept the "fat"/dynamic universal framework since it has both simulator and iphoneOS enabled. 
There are scripts that allow you to remove unused framework versions from "fat"/dynamic frameworks at compile time such as in the update section at the bottom of this article: 
http://arsenkin.com/ios-universal-framework.html
Or you can just use the iphoneOS one when building for release to the store.

## Example Usage

There are 3 ways to instantiate and run a lkqd ad. This is through automation levels. 

Example initialization:
lkqdAd = LKQDAD(theController: self, pid: (int), sid: (int), desiredBitrate: 600, automationLevel: 0)

PID and SID values are unique to each lkqd account and not optional. 
More info here: https://wiki.lkqd.com/display/PUBINT/iOS+SDK 

The default automation level is 1 where the user can initialize an ad and start it once they receive an AdLoaded event. 
Level 0 requires the user to listen to all events and respond to continue to each stage. 
Level 2 is fully automated and the ad will play as soon as possible if nothing fails. 
The desired bitrate (default is 600 kbps) is used only when automation levels 1 and 2 are used.
If you are using level 0 you will still have to pass in the bitrate again on initAd().

The events to listen to are:
webViewLoaded (required for level 0)
AdLoaded (required for level 1)

These are received in the lkqdEventReceived function which must be declared on the lkqdAd delegate. 
Delegate functions are required to know what is happening on lkqd ad. 
Most events are funneled through lkqdEventReceived but there are special cases for timeouts and ad ending.

Once the ad moves to a new state the lkqdEventReceived function will relay that event and the user can respond. 

The example app shows level 0 and level 2 being set. The default level 1 is the most common use case. 
Simply instantiate the LKQDAD with automation level 1. 
Listen for the "AdLoaded" event and call startAd() on that instance whenever you wish to show an ad to the user.

For level 0, the app shows how to manually navigate through each event. 
First lkqdAd is created with the first button press and the delegate is assigned.
The 2nd button will appear when it receives the webViewLoaded event in the delegate function lkqdEventReceived. 
When that 2nd button is pressed, the function calls lkqdAd.initAd() with the desired width/height of the ad.
Once initAd() has completed the lkqdEventReceived function will receive AdLoaded and you can press the 3rd button to call startAd()
Automation level 0 is best used where you will have time to load an ad and want a little more control over when loading and requests occur. 
You also have more control over values passed into initAd() like viewMode, creativeData, and environmentVars. 
The example app passes in defaults for these values and unless you have very custom ads you should not need to change this. 

Level 2 is much simpler and demonstrated with lkqdAd2 in the example app. Simply instantiate lkqdAd with automation level 2 and the ad will start as soon as it receives the events. 
It is important to note that you should not handle events that are automated for you since you will be simply calling functions that are already called. 
Some logic is in place to prevent this on LKQDAD classes but it should still be avoided. 
In the example app if you turn on both ads then you will notice there is a time where the first ad disappears and the 2nd ad takes a small amount of time to reappear. 
This is the drawback of automationLevel 2 since there is no way to preload the ad. 
It prepares itself and plays ASAP but it still takes time to load.
This could be avoided by using automation level 1 on the 2nd ad and preparing them both at the same time. 
Then, if you get the 2nd ad's "AdLoaded" event you could wait until the first one ends to call startAd() on the 2nd.

Another important delegate function is lkqdAdTimeOut.
If this is called then the ad has failed to load which could be for a number of reasons like an ad wasn't available or a timeout on the request.
If you receive this event you should null out your ad instance and try again. 
Automation level 1 is good for this because you either receive AdLoaded or lkqdAdTimeOut is called.
If lkqdAdTimeOut is called on an instance with automation level 2 due to it failing then you might miss an opportunity to show an ad.

Finally when an ad ends either through an error or being stopped then there will be a call to the delegate function lkqdAdEnded.
This is where you might want to transition to a new scene and null out your lkqd ad instance. 
It is strongly recommended to null out the lkqd ad instance to make sure the ad calls are unique and prevent errors.

LKQDAD has the full vpaid 2.0 interface available for accessing ad information but it will probably not be necessary to use most of those functions. 

## Important Notes

In the example app there are a few modifications to enable better ad tracking.

---Location Services---
The example app allows for location services which isn't required but the lkqd ad uses it for reporting. 
These lines are added to info.plist depending on whether location services are always on or only when in use:

<key>NSLocationAlwaysUsageDescription</key>
	<string>Turn on location services to test latitude and longitude</string>
	
<key>NSLocationWhenInUseUsageDescription</key>
	<string>Turn on location services to test latitude and longitude</string>

You can see how this is used by looking at locManager.requestAlwaysAuthorization() in ViewController.viewDidLoad()

** Location Manager Error : (KCLErrorDomain error 0) **
If you receive any location errors while running in simulator there is a solution here:
http://stackoverflow.com/a/15500603

There is no need to do anything with location services on the LKQDAD class itself. 
It should determine if location services are allowed in your app or not and act accordingly.
This section was just to explain why they are turned on in the sample app and how they are used. 

## License

© Likqid Media, Inc. - All Rights Reserved.