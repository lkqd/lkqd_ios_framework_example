//
//  MoatAdEvent.h
//  MoatMobileAppKit
//
//  Created by Moat on 2/5/16.
//  Copyright © 2016 Moat. All rights reserved.
//
//  This class is simply a data object that encapsulates info relevant to a particular playback event.

#import <Foundation/Foundation.h>

typedef enum LQDMoatAdEventType : NSUInteger {
    LQDMoatAdEventComplete
    , LQDMoatAdEventStart
    , LQDMoatAdEventFirstQuartile
    , LQDMoatAdEventMidPoint
    , LQDMoatAdEventThirdQuartile
    , LQDMoatAdEventSkipped
    , LQDMoatAdEventStopped
    , LQDMoatAdEventPaused
    , LQDMoatAdEventPlaying
    , LQDMoatAdEventVolumeChange
    , LQDMoatAdEventNone
} LQDMoatAdEventType;

static NSTimeInterval const LQDMoatTimeUnavailable = NAN;
static float const LQDMoatVolumeUnavailable = NAN;

@interface LQDMoatAdEvent : NSObject

@property LQDMoatAdEventType eventType;
@property NSTimeInterval adPlayhead;
@property float adVolume;
@property (readonly) NSTimeInterval timeStamp;

- (id)initWithType:(LQDMoatAdEventType)eventType withPlayheadMillis:(NSTimeInterval)playhead;
- (id)initWithType:(LQDMoatAdEventType)eventType withPlayheadMillis:(NSTimeInterval)playhead withVolume:(float)volume;
- (NSDictionary *)asDict;
- (NSString *)eventAsString;

@end
