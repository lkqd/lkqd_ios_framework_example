//
//  ViewController.swift
//  emptyTest
//
//  Created by noah evans on 8/25/16.
//  Copyright © 2016 lkqd. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController, LkqdDelegate
{
    var lkqdAd:LKQDAD?
    
    //example app only needed to test latitude/longitude
    //user might have another way of turning on location services
    let locManager = CLLocationManager()
    
    //example loading 2nd ad -- set this to true
    //demonstrates how to automatically cause an ad to play with only 1 call and having LKQDAD class handle the rest
    var load2Ads:Bool! = false;
    var lkqdAd2:LKQDAD?
    
    //example app specific code
    //references to buttons to control app flow
    @IBOutlet weak var _prepareButton: UIButton!
    @IBOutlet weak var _initButton: UIButton!
    @IBOutlet weak var _startButton: UIButton!
    
    //this is called when prepare button is clicked
    @IBAction func onPrepareClick(sender: AnyObject) {
        
        //--LKQD--this is all that is needed to instantiate lkqdAd webview
        //your pid and sid will be unique values. this is just an example
        lkqdAd = LKQDAD(theController: self, pid: 264, sid: 58933, desiredBitRate: 600, automationLevel: 0)
        lkqdAd!.lkqdDelegate = self //required to listen to events from lkqdAd
        
        //example app specific code
        _prepareButton.hidden = true
    }
    
    //this is called when init button is clicked
    @IBAction func onInitClick(sender: AnyObject) {
        //--LKQD--once the ad is initialized and we have received the webViewLoaded we can call initAd on it
        lkqdAd!.initAd(self.view.frame.width, height: self.view.frame.height, viewMode: "normal", desiredBitrate: 600 , creativeData: "", environmentVars: "");
        
        //example app specific code
        _initButton.hidden = true
    }
    
    //this is called when start button is clicked
    @IBAction func onStartClick(sender: AnyObject) {
        //--LKQD--call this to start an ad after you have received AdLoaded event
        lkqdAd!.startAd()
        
        //example app specific code
        _startButton.hidden = true
    }
    
    //example app specific code
    //resets buttons to original visibility
    func resetAppState()
    {
        _prepareButton.hidden = false
        _initButton.hidden = true
        _startButton.hidden = true
        self.view.backgroundColor = UIColor(red:0.16796875, green:0.62890625, blue:0.85546875, alpha:CGFloat(1.0))
    }
    
    //default auto generated function called when view controller is loaded
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //example app specific code
        resetAppState()
        locManager.requestAlwaysAuthorization() //turns on location permissions for testing latitude/longitude
    }
    
    //another default auto generated function
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //////////////////////////////////////////////////////
    //////////////LKQD DELEGATE FUNCTIONS/////////////////
    //////////////////////////////////////////////////////
    
    //LKQD AD SPECIFIC FUNCTION
    //required delegate function for handling vpaid events from lkqdad
    func lkqdEventReceived(ad: LKQDAD, event: String)
    {
        print(event)
        
        //--LKQD--event fired when the web view is done loading
        if(event == "webViewLoaded")
        {
            //at this point we know the webview is loaded and ready to receive vpaid commands like initAd()
            
            //example app specific code
            _prepareButton.hidden = true
            _initButton.hidden = false
            _startButton.hidden = true
        }
        //--LKQD--event fired when lkqdAd.initAd() is called
        if(event == "AdLoaded")
        {
            //at this point we know the ad is ready to start and is just waiting for the startAd() command
            //the ad can now be resized and have other vpaid calls to it
            
            //example app specific code
            _prepareButton.hidden = true
            _initButton.hidden = true
            _startButton.hidden = false
        }
        
        //--LKQD--example of progress event
        //maybe you want to start loading something in the background since you have time
        if(event == "AdVideoFirstQuartile")
        {
            
        }
        
        //--LKQD--ad has been clicked and safari should now be loading their site in the foreground
        //Might want to call stopAd() and transition to the next scene when "lkqdAdEnded" gets called
        if(event == "AdClickThru")
        {
            lkqdAd!.stopAd() //calling this will cause the ad to end and "lkqdAdEnded" function will be called
        }
        
    }
    
    //LKQD AD SPECIFIC FUNCTION
    //required delegate function called after 5 second timeout on initAd() call
    func lkqdAdTimeOut(ad: LKQDAD)
    {
        //handle ad not loading properly
        //null out instance of lkqd ad (and try again if you want), switch to next scene, etc
        lkqdAd = nil;
        resetAppState();
    }
    
    //LKQD AD SPECIFIC FUNCTION
    //required delegate function called when the ad has stopped or has an error
    func lkqdAdEnded(ad: LKQDAD)
    {
        //this function is called when AdStopped or AdError has fired
        //This should be an entry point to change scenes since the ad is no longer visible
        //Or you could play another ad here if so desired
        
        //example 2nd ad auto starting based on event
        //since the first ad has fired the lkqdAdEnded event we can move onto the next
        if(load2Ads!)
        {
            if(ad == lkqdAd)
            {
                //ad will auto start
                lkqdAd2 = LKQDAD(theController: self, pid: 264, sid: 58933, desiredBitRate: 600, automationLevel:2)
                lkqdAd2!.lkqdDelegate = self
            }
            if(ad == lkqdAd2)
            {
                lkqdAd2 = nil;
            }
        }
        
        //--LKQD--null out lkqd ad when complete.
        //Should reinstance it each time to make sure web view loads with new ad information
        lkqdAd = nil;
        
        //example app specific code
        resetAppState()
    }
    
    //LKQD AD SPECIFIC FUNCTION
    //required delegate function for handling error on lkqd ad loading webview
    //just adding it so the user doesn't get hung up in unlikely event that lkqdAd webView has an error
    //highly unlikely this will be hit
    func lkqdAdWebviewError(ad: LKQDAD, didFailLoadWithError error: NSError?)
    {
        print("lkqdAd webview fail with error \(error)");
    }
    
    
    
}

